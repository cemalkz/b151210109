﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b151210109.Models;

namespace b151210109.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        { 
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Haber()
        {
            return View();
        }

        public ActionResult Yasuo()
        {
            return View();
        }

        public ActionResult Ahri()
        {
            return View();
        }

        public ActionResult Azir()
        {
            return View();
        }

        public ActionResult LoLNedir()
        {
            return View();
        }

        public ActionResult OyunModu()
        {
            return View();
        }

        public ActionResult İndirim()
        {
            return View();
        }

        public ActionResult Paket()
        {
            return View();
        }

        public ActionResult Rotasyon()
        {
            return View();
        }

        public ActionResult Camille()
        {
            return View();
        }

        public ActionResult Poro()
        {
            return View();
        }
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
    }
}