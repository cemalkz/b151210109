﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(b151210109.Startup))]
namespace b151210109
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
